package poo.rapturepark.drivercontroller.democontroller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemocontrollerApplication {

    public static void  main(String[] args) {
        SpringApplication.run(DemocontrollerApplication.class, args);
    }

}
