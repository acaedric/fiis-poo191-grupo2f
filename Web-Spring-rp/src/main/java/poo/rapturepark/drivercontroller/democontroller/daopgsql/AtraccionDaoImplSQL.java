package poo.rapturepark.drivercontroller.democontroller.daopgsql;

import com.sun.org.apache.xalan.internal.xsltc.util.IntegerArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import poo.rapturepark.drivercontroller.democontroller.idao.IAtraccionDao;
import poo.rapturepark.drivercontroller.democontroller.model.Atraccion;

import java.sql.*;
import java.util.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


import java.util.ArrayList;
import java.util.List;

public class AtraccionDaoImplSQL implements IAtraccionDao {
    @Autowired
    JdbcTemplate template;
    //lista de tipo atraccion
    List<Atraccion> atraccions;

    //inicializar los objetos cliente y añadirlos a la lista
    public AtraccionDaoImplSQL() {
        atraccions = new ArrayList<>();
        Atraccion atraccion1 = new Atraccion(1,"Carrusel de caballos",10,2,"Holanda","Carrusel");
        Atraccion atraccion2 = new Atraccion(2,"Dragon fly",16,5,"Alemania","Montaña rusa");
        atraccions.add(atraccion1);
        atraccions.add(atraccion2);
    }

    // insertar Atraccion
    @Override
    public void addAtraccion(Atraccion atraccion) throws SQLException {
        String sql = "INSERT INTO atraccion (id, nombre, edad_restringida, nivel_intensidad, lugar_disponible, tipo_atraccion,valoraciontotal,numvaloraciones) VALUES (?, ?, ?,?,?,?,?,?)";
        System.out.println(atraccion.getNombre());
        Connection conn = template.getDataSource().getConnection();
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, atraccion.getId());
        statement.setString(2, atraccion.getNombre());
        statement.setInt(3, atraccion.getEdadRestringida());
        statement.setInt(4, atraccion.getNivelIntensidad());
        statement.setString(5, atraccion.getLugarDisponible());
        statement.setString(6, atraccion.getTipo());
        statement.setDouble(7, atraccion.getValoraciontotal());
        statement.setInt(8, atraccion.getNumerodevaloraciones());

        boolean rowInserted = statement.executeUpdate() > 0;
        statement.close();
        conn.close();
    }

    //obtener todas las atracciones
    @Override
    public List<Atraccion> obtenerAtracciones() throws SQLException{
        List<Atraccion> listaAtracciones = new ArrayList<Atraccion>();
        Connection conn = template.getDataSource().getConnection();
        String sql = "SELECT * FROM atraccion";
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while(rs.next()){
            int id = rs.getInt(1);
            String nombre = rs.getString(2);
            int edadrestringida = rs.getInt(3);
            int nivelintensidad = rs.getInt(4);
            String lugardisponible = rs.getString(5);
            String tipo = rs.getString(6);
            double valoraciontotal = rs.getDouble(7);
            int numvaloraciones = rs.getInt(8);

            Atraccion atraccion = new Atraccion(id, nombre, edadrestringida, nivelintensidad, lugardisponible, tipo,valoraciontotal,numvaloraciones);
            listaAtracciones.add(atraccion);
            System.out.println("Atraccion " + nombre + "en : " + lugardisponible + " generada.");

        }
        return listaAtracciones;
    }

    //obtener una atraccion por el id
    @Override
    public Atraccion obtenerAtraccion(int id) throws SQLException {
        Atraccion atraccion = null;
        String sql = "SELECT * FROM atraccion WHERE id= ? ";
        Connection conn = template.getDataSource().getConnection();
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        if (rs.next()) {
            atraccion = new Atraccion(rs.getInt(1), rs.getString(2), rs.getInt(3),
                    rs.getInt(4), rs.getString(5), rs.getString(6),rs.getDouble(7),rs.getInt(8));
        }
        rs.close();
        conn.close();

        return atraccion;
    }

    //actualizar una atraccion
    @Override
    public void actualizarAtraccion(Atraccion atraccion) throws SQLException {
        String sql = "UPDATE atraccion SET nombre=?,edadrestringida=?,nivelintensidad=?,lugardisponible=?, tipo=? WHERE id=?";
        Connection conn = template.getDataSource().getConnection();
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, atraccion.getNombre());
        statement.setInt(2, atraccion.getEdadRestringida());
        statement.setInt(3, atraccion.getNivelIntensidad());
        statement.setString(4, atraccion.getLugarDisponible());
        System.out.println(atraccion.getTipo());
        statement.setString(5, atraccion.getTipo());
        statement.setInt(6, atraccion.getId());
        statement.close();
        conn.close();
        System.out.println("Atraccion con id: "+atraccion.getId()+" actualizado satisfactoriamente");
    }

    //eliminar una atraccion por el id
    @Override
    public void eliminarAtraccion(Atraccion atraccion) throws SQLException {
        boolean rowEliminar = false;
        String sql = "DELETE FROM atraccion WHERE id=?";
        Connection conn = template.getDataSource().getConnection();
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, atraccion.getId());
        atraccions.remove(atraccion.getId());
        statement.close();
        conn.close();

        System.out.println("Atraccion con id: "+atraccion.getId()+" elimnado satisfactoriamente");
    }

}
