package poo.rapturepark.drivercontroller.democontroller.model;

import java.util.Scanner;

public class ServidorRegistro {
    Scanner sc = new Scanner(System.in);
    private int edad;
    private String nombre;
    private String apellido;
    private String username;
    private String contraseno;
    private String lineaBancaria;

    public ServidorRegistro() {
    }

    public void modificar_registro(){
        System.out.print("Nuevo nombre: ");
        String nn = sc.nextLine();
        System.out.print("Nuevo apellidos: ");
        String na = sc.nextLine();
        System.out.print("Nuevo username: ");
        String nu = sc.nextLine();
        System.out.print("Nueva contraseña: ");
        String nc = sc.nextLine();
        System.out.print("Nuevo edad: ");
        int ne = sc.nextInt();
        System.out.print("Nueva Linea Bancaria: ");
        String nlb = sc.nextLine();
        System.out.printf("=====================");
        setNombre(nn);
        setApellido(na);
        setUsername(nu);
        setContraseno(nc);
        setEdad(ne);
        setLineaBancaria(nlb);
    }

    public void mostrar_registro(){
        System.out.println(getUsername());
        System.out.println(getContraseno());
        System.out.println(getNombre());
        System.out.println(getApellido());
        System.out.println(getEdad());
        System.out.println(getLineaBancaria());


    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContraseno() {
        return contraseno;
    }

    public void setContraseno(String contraseno) {
        this.contraseno = contraseno;
    }

    public String getLineaBancaria() {
        return lineaBancaria;
    }

    public void setLineaBancaria(String  lineaBancaria) {
        this.lineaBancaria = lineaBancaria;
    }
}
