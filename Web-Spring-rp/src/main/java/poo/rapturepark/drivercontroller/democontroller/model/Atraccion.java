package poo.rapturepark.drivercontroller.democontroller.model;

import java.sql.*;

public class Atraccion {
    private int id;
    private String nombre;
    private int edadRestringida;
    private int nivelIntensidad;
    private String lugarDisponible;
    private String tipo;
    private double valoraciontotal;
    private int numerodevaloraciones;


    public Atraccion(){
        super();
    }


    public Atraccion(int id,String nombre, int edadRestringida, int nivelIntensidad, String lugarDisponible, String tipo) {
        this.id=id;
        this.nombre=nombre;
        this.edadRestringida=edadRestringida;
        this.nivelIntensidad=nivelIntensidad;
        this.lugarDisponible=lugarDisponible;
        this.tipo=tipo;
        this.valoraciontotal=0;
        this.numerodevaloraciones=0;
    }
    public Atraccion(int id,String nombre, int edadRestringida, int nivelIntensidad, String lugarDisponible, String tipo, double valoraciontotal,int numerodevaloraciones) {
        this.id=id;
        this.nombre=nombre;
        this.edadRestringida=edadRestringida;
        this.nivelIntensidad=nivelIntensidad;
        this.lugarDisponible=lugarDisponible;
        this.tipo=tipo;
        this.valoraciontotal=valoraciontotal;
        this.numerodevaloraciones=numerodevaloraciones;
    }

    public void addAtraccion(Connection connection) throws ClassNotFoundException {
        try {
            String sql = "INSERT INTO ATRACCION VALUES (?,?,?,?,?,?)";
            PreparedStatement pst = connection.prepareStatement(sql);
            pst.setInt(1,id);
            pst.setString(2,nombre);
            pst.setInt(3,edadRestringida);
            pst.setInt(4,nivelIntensidad);
            pst.setString(5,lugarDisponible);
            pst.setString(6,tipo);
            pst.executeUpdate();

            pst.close();

        } catch(SQLException e ){
            System.out.println("Error SQL....");
            e.printStackTrace();
        }

    }

    public int getEdadRestringida() {
        return edadRestringida;
    }

    public void getdataEdadRestringida(Connection connection){
        try {
            String sql = "SELECT nombre,edad_restringida FROM atraccion WHERE ID = " + id;
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                String nombre = rs.getString(1);
                int edad = rs.getInt(2);
                System.out.println("Edad restringida para el parque "+nombre+" : " + edad);
            }
            st.close();

        } catch(SQLException e ){
            System.out.println("Error SQL....");
            e.printStackTrace();
        }
    }
    public void addvaloracion(int puntaje){
        this.numerodevaloraciones++;
        this.valoraciontotal=((this.numerodevaloraciones-1)*this.valoraciontotal+puntaje)/this.numerodevaloraciones;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEdadRestringida(int edadRestringida) {
        this.edadRestringida = edadRestringida;
    }

    public int getNivelIntensidad() {
        return nivelIntensidad;
    }

    public void setNivelIntensidad(int nivelIntensidad) {
        this.nivelIntensidad = nivelIntensidad;
    }

    public String getLugarDisponible() {
        return lugarDisponible;
    }

    public void setLugarDisponible(String lugarDisponible) {
        this.lugarDisponible = lugarDisponible;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getValoraciontotal() {
        return valoraciontotal;
    }

    public void setValoraciontotal(double valoraciontotal) {
        this.valoraciontotal = valoraciontotal;
    }

    public int getNumerodevaloraciones() {
        return numerodevaloraciones;
    }

    public void setNumerodevaloraciones(int numerodevaloraciones) {
        this.numerodevaloraciones = numerodevaloraciones;
    }
}
