package poo.rapturepark.drivercontroller.democontroller;
import com.stripe.Stripe;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import poo.rapturepark.drivercontroller.democontroller.stripe.Response;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ProyectoController{
    @Autowired
    JdbcTemplate template;

    @PostMapping("/proyecto/create")
    public ResponseEntity registrarProyecto
            (@RequestParam String id_producto) throws Exception {
        Connection conn = template.getDataSource().getConnection();
        String query = "SELECT * FROM product_information WHERE product_id = ?";
        PreparedStatement pst = conn.prepareStatement(query);
        pst.setInt(1, Integer.parseInt(id_producto));
        ResultSet rs = pst.executeQuery();
        rs.next();
        String nombre_producto = rs.getString(2);
        Float precioactual = rs.getFloat(9);
        System.out.println("El producto " + nombre_producto + " tiene el precio: " + Float.toString(precioactual));
        String sql = "UPDATE product_information SET list_price = ?";
        PreparedStatement pst2 = conn.prepareStatement(sql);
        pst2.setFloat(1, precioactual * 110 / 100);
        pst2.executeUpdate();
        return new ResponseEntity("Correcto", HttpStatus.OK);
    }
    @PostMapping("/shop/tickets")
    public String costearTickets
            (@RequestParam String num_ninos, String num_adultos, String num_ancianos) throws Exception {
        Connection conn = template.getDataSource().getConnection();
        String query = "INSERT INTO costeartickets(n_ninos,n_adultos,n_ancianos,precio) VALUES (?,?,?,?)";
        PreparedStatement pst = conn.prepareStatement(query);
        pst.setInt(1, Integer.parseInt(num_ninos));
        pst.setInt(2, Integer.parseInt(num_adultos));
        pst.setInt(3, Integer.parseInt(num_ancianos));
        pst.setDouble(4, ((Integer.parseInt(num_ninos) + Integer.parseInt(num_ancianos)) * 44.5 + Integer.parseInt(num_adultos) * 52.5));
        pst.executeUpdate();
        return "Correcto";
    }
    @PostMapping("/login")
    public String verificarlogin
            (@RequestParam String email, String password) throws Exception {
        Connection conn = template.getDataSource().getConnection();
        String query = "SELECT * FROM usuario WHERE (correo,password)=(?,?) ";
        PreparedStatement pst = conn.prepareStatement(query);
        pst.setString(1, (email));
        pst.setString(2, (password));
        ResultSet rs = pst.executeQuery();
        if (rs.next() == false){
            return "Contraseña o email no concuerda";
        } else {
            return "Has inciado sesión";
        }
    }
    @PostMapping("/profile")
    public String registrarse
            (@RequestParam String nombre, String apellidos, String aniversario,String email, String telefono, String password) throws Exception {
        Connection conn = template.getDataSource().getConnection();
        String query = "INSERT INTO usuario(nombre,apellido,correo,telefono,tipo_usuario,password) VALUES (?,?,?,?,?,?)";
        PreparedStatement pst = conn.prepareStatement(query);
        pst.setString(1, (nombre));
        pst.setString(2, (apellidos));
        pst.setString(3, (email));
        pst.setString(4, (telefono));
        pst.setString(5,  "Cliente");
        pst.setString(6, password);
        pst.executeUpdate();
        return "Registro exitoso";
    }
    @PostMapping("/checkout")
    public @ResponseBody
    Response compraviaStripe
            (@RequestParam String stripeEmail, String stripeToken) throws Exception{
        Stripe.apiKey = "sk_test_5cvmdt92972In1hFzOkp728V00VfVWtoSd";
        Map<String,Object> customerParameter = new HashMap<String,Object>();
        customerParameter.put("email",stripeEmail);
        customerParameter.put("source",stripeToken);
        Customer newCustomer = Customer.create(customerParameter);
        System.out.println(newCustomer.getId());

        Map<String,Object> chargeParam = new HashMap<String,Object>();
        chargeParam.put("amount","28950");
        chargeParam.put("currency","usd");
        chargeParam.put("customer",newCustomer.getId());
        chargeParam.put("description","Ticket 1 Zona por día 2019");

        Charge.create(chargeParam);
        return new Response(true, "Satisfactorio! Tu cargo es de 289 dólares");
    }



}