package poo.rapturepark.drivercontroller.democontroller.model;

public class Perfil {
    private String nombre;
    private String apellido;
    private String contraseña;
    private int edad;
    private String contacto;
    private String lineaBancaria;
    private String producto;
    private String username;
    private int id_usuario;



    public Perfil(int id_usuario,String nombre, String apellido,String username, String contraseña,int edad, String lineaBancaria) {
        this.id_usuario = id_usuario;
        this.nombre = nombre;
        this.apellido = apellido;
        this.username = username;
        this.contraseña = contraseña;
        this.edad = edad;
        this.lineaBancaria = lineaBancaria;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public String getNombre(){
        return nombre;}

    public String getApellido() {
        return apellido;
    }

    public String getContraseña(){
        return contraseña;}

    public int getEdad(){
        return edad;}
    public String getContacto(){
        return contacto;}

    public String getLineaBancaria(){
        return lineaBancaria;}

    public String getProducto(){
        return producto;}

    public String getUsername(){
        return username;}

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public void setLineaBancaria(String lineaBancaria) {
        this.lineaBancaria = lineaBancaria;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public void setFullName(String fullName) {
        this.username = fullName;
    }
}

