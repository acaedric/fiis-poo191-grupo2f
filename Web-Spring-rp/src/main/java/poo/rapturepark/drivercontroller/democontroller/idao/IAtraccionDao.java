package poo.rapturepark.drivercontroller.democontroller.idao;
import java.sql.SQLException;
import java.util.List;
import poo.rapturepark.drivercontroller.democontroller.model.Atraccion;
public interface IAtraccionDao {
    //declaración de métodos para acceder a la base de datos

    public void addAtraccion(Atraccion atraccion) throws SQLException;
    public List<Atraccion> obtenerAtracciones() throws SQLException;
    public Atraccion obtenerAtraccion(int id) throws SQLException;
    public void actualizarAtraccion(Atraccion atraccion) throws SQLException;
    public void eliminarAtraccion(Atraccion atraccion) throws SQLException;
}
