package poo.rapturepark.drivercontroller.democontroller.model;

public class Reserva {
    private int id_usuario;
    private int codigo;
    private int fechaIngreso;
    private int fechaSalida;
    private int cantidad;
    private Habitacion habitacion;



    public Reserva(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo() {
        return codigo;
    }

    public int getFechaIngreso() {
        return fechaIngreso;
    }

    public int getFechaSalida() {
        return fechaSalida;
    }

    public int getCantidad() {
        return cantidad;
    }

    public Habitacion getHabitacion() {
        return habitacion;
    }

    public void verificarReserve(){
    }
    public void registrarReserve(int id_usuario,int fechaIngreso, int fechaSalida, int cantidad, Habitacion habitacion) {
        this.id_usuario=id_usuario;
        this.fechaIngreso = fechaIngreso;
        this.fechaSalida = fechaSalida;
        this.cantidad = cantidad;
        this.habitacion = habitacion;
    }
}
