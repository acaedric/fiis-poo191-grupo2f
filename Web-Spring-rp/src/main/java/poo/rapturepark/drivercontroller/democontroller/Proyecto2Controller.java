package poo.rapturepark.drivercontroller.democontroller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Controller
public class Proyecto2Controller {
    @Autowired
    JdbcTemplate template;
    @GetMapping("/")
    public String index() {
        return "main.html";
    }
    @GetMapping("/comprartickets")
    public String comprartickets(){
        return "shop-tickets.html";
    }
    @GetMapping("/register")
    public String registrar(){
        return "registro.html";
    }
    @GetMapping("/atracciones")
    public String atracciones(){
        return "atracciones.html";
    }
    @GetMapping("/alojamiento")
    public String alojamiento(){
        return "alojamiento.html";
    }
    @GetMapping("/comidaybebida")
    public String comidaybebida(){return "comidaybebida.html";}
    @GetMapping("/precios")
    public String precios(){return "precios.html";}
    @GetMapping("/tickets")
    public String tickets(){return "precios.html";}
    @GetMapping("/login")
    public String login(){return "login.html";}

    @GetMapping("/login#")
    public String verificarlogin
            (@RequestParam String email, String password) throws Exception {
        Connection conn = template.getDataSource().getConnection();
        String query = "SELECT * FROM usuarios WHERE (nombreusuario,passwordusuario)=(?,?) ";
        PreparedStatement pst = conn.prepareStatement(query);
        pst.setString(1, (email));
        pst.setString(2, (password));
        ResultSet rs = pst.executeQuery();
        System.out.println("HoLA amigos");
        if (rs.next() == false){
            return "login.html";
        } else {
            return "main.html";
        }
    }


}