package poo.rapturepark.drivercontroller.democontroller.daodemo;

import poo.rapturepark.drivercontroller.democontroller.dao.AtraccionDaoImpl;
import poo.rapturepark.drivercontroller.democontroller.idao.IAtraccionDao;
import poo.rapturepark.drivercontroller.democontroller.model.Atraccion;

import java.sql.SQLException;

public class DaoDemoAtraccion {
    public static void main(String[] args) throws SQLException {
        // objeto para manipular el dao
        IAtraccionDao atraccionDao = new AtraccionDaoImpl();

        // imprimir los clientes
        atraccionDao.obtenerAtracciones().forEach(System.out::println);

        // obtner un cliente
        Atraccion atraccion = atraccionDao.obtenerAtraccion(1);
        atraccion.setNombre("Breath dragon");
        //actualizar cliente
        atraccionDao.actualizarAtraccion(atraccion);

        // imprimir los clientes
        System.out.println("*****");
        atraccionDao.obtenerAtracciones().forEach(System.out::println);
    }
}
