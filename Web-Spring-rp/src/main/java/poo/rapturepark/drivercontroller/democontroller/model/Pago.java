package poo.rapturepark.drivercontroller.democontroller.model;

public class Pago {
    private int creditCard;
    private int idPago;
    private int num_referencia_pago;
    private float cantidadPagar;

    public Pago(int idPago) {
        this.idPago = idPago;
    }

    public int getCreditCard() {
        return creditCard;
    }

    public int getIdPago() {
        return idPago;
    }

    public int getNum_referencia_pago() {
        return num_referencia_pago;
    }

    public float getCantidadPagar() {
        return cantidadPagar;
    }

    public void registrarPago(){
    }
    public void verificarPago(){
    }
}
