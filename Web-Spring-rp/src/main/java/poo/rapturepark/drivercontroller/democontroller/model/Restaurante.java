package poo.rapturepark.drivercontroller.democontroller.model;

public class Restaurante {
    private int id;
    private String nombre;
    private String contacto;
    private String direccion;
    private String descripcion;

    public Restaurante(int id, String nombre, String contacto, String direccion, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.contacto = contacto;
        this.direccion = direccion;
        this.descripcion = descripcion;
    }

    public void mostrar_datos(){
        System.out.println(nombre);
        System.out.println(contacto);
        System.out.println(descripcion);
        System.out.println(direccion);
    }

    public String getNombre() {
        return nombre;
    }

    public String getContacto() {
        return contacto;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
