package poo.rapturepark.drivercontroller.democontroller.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {
    public Connection connection;
    /**
     * Establecemos la conexión con la base de datos <b>rapturepark</b>.
     */
    public void connectDatabase() {
        try {
            // Registramos el driver de PostgresSQL
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex) {
                System.out.println("Error al registrar el driver de PostgreSQL: " + ex);
            }
            Connection connection = null;
            // Database connect
            // Conectamos con la base de datos
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/rapturepark");

            boolean valid = connection.isValid(50000);
            System.out.println(valid ? "TEST OK" : "TEST FAIL");
            this.connection=connection;
        } catch (SQLException sqle) {
            System.out.println("Error: " + sqle);
        }
    }

    /**
     * Método para establecer la conexión a la base de datos mediante el paso de parámetros.
     *
     * @param host <code>String</code> host name or ip. Nombre del host o ip.
     * @param port <code>String</code> listening database port. Puerto en el que escucha la base de datos.
     * @param database <code>String</code> database name for the connection. Nombre de la base de datos para la conexión.
     * @param user <code>String</code> user name. Nombre de usuario.
     * @param password  <code>String</code> user password. Password del usuario.
     */
    public void connectDatabase(String host, String port, String database,
                                String user, String password) {
        String url = "";
        try {
            // We register the PostgreSQL driver
            // Registramos el driver de PostgresSQL
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex) {
                System.out.println("Error al registrar el driver de PostgreSQL: " + ex);
            }
            Connection connection = null;
            url = "jdbc:postgresql://" + host + ":" + port + "/" + database;
            // Database connect
            // Conectamos con la base de datos
            connection = DriverManager.getConnection(
                    url,
                    user, password);
            boolean valid = connection.isValid(50000);
            System.out.println(valid ? "TEST OK" : "TEST FAIL");
        } catch (SQLException sqle) {
            System.out.println("Error al conectar con la base de datos de PostgreSQL (" + url + "): " + sqle);
        }
    }

    /**
     * Testing Java PostgreSQL connection with host and port
     * Probando la conexión en Java a PostgreSQL especificando el host y el puerto.
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Main javaPostgreSQLBasic = new Main();
        javaPostgreSQLBasic.connectDatabase();
        Connection conexion = javaPostgreSQLBasic.connection;
        Atraccion atraccion1 = new Atraccion(0,"Carrusel de caballos",10,2,"Siempre","Carrusel");

        atraccion1.addAtraccion(conexion);
        atraccion1.getdataEdadRestringida(conexion);
        Producto producto1 = new Producto(3,"ticket","ticket para aparcamiento zona 2","Cercanía a la zona sur este del parque",25.5);
        producto1.addProducto(conexion);
        Visitante visitante1 = new Visitante(conexion);
        visitante1.registrarse(conexion);

        conexion.close();




    }
}