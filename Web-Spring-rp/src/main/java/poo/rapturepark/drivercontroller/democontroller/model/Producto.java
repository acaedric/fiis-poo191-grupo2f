package poo.rapturepark.drivercontroller.democontroller.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Producto{
    private int id;
    private String tipo;
    private String nombre;
    private String descripcion;
    private double costo_por_unidad;

    public Producto(int id, String tipo, String nombre, String descripcion, double costo_por_unidad) {
        this.id = id;
        this.tipo = tipo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.costo_por_unidad = costo_por_unidad;
    }

    public void addProducto(Connection connection) throws ClassNotFoundException {
        try {
            String sql = "INSERT INTO producto(tipo_producto,nombre_producto,costo,descripcion) VALUES (?,?,?,?)";
            PreparedStatement pst = connection.prepareStatement(sql);
            pst.setString(1,tipo);
            pst.setString(2,nombre);
            pst.setDouble(3,costo_por_unidad);
            pst.setString(4,descripcion);
            pst.executeUpdate();

            pst.close();

        } catch(SQLException e ){
            System.out.println("Error SQL....");
            e.printStackTrace();
        }
    }

    public int getId(){
        return id;
    }

    public String getNombre(){
        return nombre;
    }

    public String getTipo(){
        return tipo;
    }
}
