package poo.rapturepark.drivercontroller.democontroller.dao;
import poo.rapturepark.drivercontroller.democontroller.model.Atraccion;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import poo.rapturepark.drivercontroller.democontroller.idao.IAtraccionDao;

public class AtraccionDaoImpl implements IAtraccionDao {
    //lista de tipo atraccion
    List<Atraccion> atraccions;
    //inicializar los objetos cliente y añadirlos a la lista
    public AtraccionDaoImpl() {
        atraccions = new ArrayList<>();
        Atraccion atraccion1 = new Atraccion(1,"Carrusel de caballos",10,2,"Holanda","Carrusel");
        Atraccion atraccion2 = new Atraccion(2,"Dragon fly",16,5,"Alemania","Montaña rusa");
        atraccions.add(atraccion1);
        atraccions.add(atraccion2);
    }

    @Override
    public void addAtraccion(Atraccion atraccion) throws SQLException {

    }

    //obtener todas las atracciones
    @Override
    public List<Atraccion> obtenerAtracciones() {
        return atraccions;
    }

    //obtener una atraccion por el id
    @Override
    public Atraccion obtenerAtraccion(int id) {
        return atraccions.get(id);
    }

    //actualizar una atraccion
    @Override
    public void actualizarAtraccion(Atraccion atraccion) {
        atraccions.get(atraccion.getId()).setNombre(atraccion.getNombre());
        atraccions.get(atraccion.getId()).setEdadRestringida(atraccion.getEdadRestringida());
        System.out.println("Atraccion con id: "+atraccion.getId()+" actualizado satisfactoriamente");
    }

    //eliminar una atraccion por el id
    @Override
    public void eliminarAtraccion(Atraccion atraccion) {
        atraccions.remove(atraccion.getId());
        System.out.println("Atraccion con id: "+atraccion.getId()+" elimnado satisfactoriamente");
    }

}
