package poo.rapturepark.drivercontroller.democontroller.model;

public class RegistroHuespedes {
    private int id;
    private int id_usuario;
    private int fechaLlegada;
    private int fechaSalida;
    private int cantidadPersonas;

    public RegistroHuespedes(int id, int id_usuario, int fechaLlegada, int fechaSalida, int cantidadPersonas) {
        this.id = id;
        this.id_usuario = id_usuario;
        this.fechaLlegada = fechaLlegada;
        this.fechaSalida = fechaSalida;
        this.cantidadPersonas = cantidadPersonas;
    }


    public void mostrardatos(){
        System.out.println(this.id_usuario);
        System.out.println(fechaLlegada);
        System.out.println(fechaSalida);
        System.out.println(cantidadPersonas);
    }

    public int getFechaLlegada() {
        return fechaLlegada;
    }

    public int getFechaSalida() {
        return fechaSalida;
    }

    public int getCantidadPersonas() {
        return cantidadPersonas;
    }
}

