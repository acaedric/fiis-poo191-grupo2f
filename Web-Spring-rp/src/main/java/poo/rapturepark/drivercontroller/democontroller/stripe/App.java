package poo.rapturepark.drivercontroller.democontroller.stripe;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.Token;

public class App {
    public static void main(String[] args) throws StripeException {
        Stripe.apiKey = "sk_test_5cvmdt92972In1hFzOkp728V00VfVWtoSd";

        //Crear nuevo customer
        /*Map<String,Object> customerParameter = new HashMap<String,Object>();
        customerParameter.put("email","a@gmail.com");
        Customer newCustomer = Customer.create(customerParameter);
        System.out.println(newCustomer.getId());*/

        //Llamar a un customer
        Customer a = Customer.retrieve("cus_FLoid0qsV4O7Lw");

        //Poner datos de la tarjeta del customer
        /*Map<String,Object> cardParam = new HashMap<String,Object>();
        cardParam.put("number","4242424242424242");
        cardParam.put("exp_month","11");
        cardParam.put("exp_year","2023");
        cardParam.put("cvc","123");
        
        Map <String,Object> tokenParam = new HashMap<String,Object>();
        tokenParam.put("card", cardParam);

        Token token = Token.create(tokenParam);

        Map <String,Object> source = new HashMap<String, Object>();
        source.put("source",token.getId());

        a.getSources().create(source);*/

        //Generar un cargo o pago del customer(Cliente)

        /*Map<String,Object> chargeParam = new HashMap<String,Object>();
        chargeParam.put("amount","500");
        chargeParam.put("currency","usd");
        chargeParam.put("customer",a.getId());

        Charge.create(chargeParam);*/


        //Ver datos del customer y sacar su json
        /*System.out.println(a);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(gson.toJson(a));*/

        //Poner datos de la tarjeta con Token
        Map<String,Object> cardParam = new HashMap<String,Object>();
        cardParam.put("number","4242424242424242");
        cardParam.put("exp_month","11");
        cardParam.put("exp_year","2025");
        cardParam.put("cvc","123");

        Map <String,Object> tokenParam = new HashMap<String,Object>();
        tokenParam.put("card", cardParam);

        Token token = Token.create(tokenParam);

        Map <String,Object> source = new HashMap<String, Object>();
        source.put("source",token.getId());

        a.getSources().create(source);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(gson.toJson(token));

        Map<String,Object> chargeParam = new HashMap<String,Object>();
        chargeParam.put("amount","500");
        chargeParam.put("currency","usd");
        chargeParam.put("source",token.getId());

        Charge.create(chargeParam);





    }
}
