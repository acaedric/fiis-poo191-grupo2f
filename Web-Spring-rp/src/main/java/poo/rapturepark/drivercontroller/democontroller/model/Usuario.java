package poo.rapturepark.drivercontroller.democontroller.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

//Poner esto como clase hija de Producto
public class Usuario extends Visitante {
    Scanner sc = new Scanner(System.in);
    private Perfil perfil;
    private ArrayList<Producto> productos = new ArrayList<>();
    private ArrayList<Integer> listaCantidad = new ArrayList<>();


    public Usuario(Perfil perfil, Connection connection) {
        this.perfil= perfil;
        try {
            String sql = "INSERT INTO usuario(nombre,apellido,edad,telefono,usuario,password) VALUES (?,?,?,?,?,?)";
            PreparedStatement pst = connection.prepareStatement(sql);
            pst.setString(1,this.perfil.getNombre());
            pst.setString(2,this.perfil.getApellido());
            pst.setInt(3,this.perfil.getEdad());
            pst.setString(4,this.perfil.getContacto());
            pst.setString(5,this.perfil.getUsername());
            pst.setString(6,this.perfil.getContraseña());


            pst.executeUpdate();

            pst.close();

        } catch(SQLException e ){
            System.out.println("Error SQL....");
            e.printStackTrace();
        }

    }


    public void comprarProducto(Producto producto) {
        System.out.printf("¿Cuántos deseas comprar?%d", producto.getNombre());
        Integer cantProductos = sc.nextInt();
        productos.add(producto);
        listaCantidad.add(cantProductos);


    }

    public void reservaHabitacion(){

    }

    public void editarPerfil(){
        Scanner entrada = new Scanner(System.in);
        System.out.println("Escriba su correo de contacto");
        String nombre = entrada.nextLine();
        String password = entrada.nextLine();
        int edad = entrada.nextInt();
        String contact = entrada.nextLine();
        String lineaB  = entrada.nextLine();
        String producto = entrada.nextLine();
        String name = entrada.nextLine();

        this.perfil.setNombre(nombre);
        this.perfil.setContraseña(password);
        this.perfil.setEdad(edad);
        this.perfil.setContacto(contact);
        this.perfil.setLineaBancaria(lineaB);
        this.perfil.setProducto(producto);
        this.perfil.setFullName(name);


    }

    public void productoAdquiridos(){
        for(int i=0;i<productos.size();i++){
            System.out.println(productos.get(i).getNombre());
        }
    }

    public void ofertas(){

    }

    public void cosultar(){

    }
    public void hoteles(){

    }
    public void atracciones(){

    }
    public void preferenciaPago(){

    }
    public void reservas(){

    }
}
