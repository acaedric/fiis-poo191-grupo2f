package poo.rapturepark.drivercontroller.democontroller.model;

import java.sql.*;
import java.util.Scanner;

public class Visitante {
    private int tempoActivo;
    public Visitante (Connection connection) {
        tempoActivo=0;
        try {
            String sql = "INSERT INTO visitantes(tempoActivo) VALUES (?)";
            PreparedStatement pst = connection.prepareStatement(sql);

            pst.setInt(1,tempoActivo);
            pst.executeUpdate();

            pst.close();

        } catch(SQLException e ){
            System.out.println("Error SQL....");
            e.printStackTrace();
        }

    }

    public Visitante() {

    }

    public void buscarProductos(){

    }
    public void buscarHoteles(){

    }
    public void verAtracciones(){

    }
    public void registrarse(Connection connection){
        ServidorRegistro registro = new ServidorRegistro();
        Scanner entrada = new Scanner(System.in);
        /*Modificar registro*/
        registro.modificar_registro();
        /*Confirmacion registro*/
        registro.mostrar_registro();
        System.out.printf("Desea modificar?");
        String confirmacion =entrada.nextLine();
        if (confirmacion=="Si"){
            registro.modificar_registro();
        } else {

        }
        try {
            int cantColumnas = 0;
            String sql = "select * from usuario";
            Statement st = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = st.executeQuery(sql);
            cantColumnas = rs.getMetaData().getColumnCount();
            st.close();
            Perfil perfil = new Perfil(cantColumnas+1,registro.getNombre(),registro.getApellido(),registro.getUsername(),registro.getContraseno(),registro.getEdad(),registro.getLineaBancaria());
            Usuario nuevo_usuario = new Usuario(perfil,connection);
        } catch(SQLException e ){
            System.out.println("Error SQL....");
            e.printStackTrace();
        }
        ;

    }
    public void verRestaurantes(){

    }
    public void consultar(){

    }
    public void iniciarSesion(String usuario, String contraseña){

    }

}