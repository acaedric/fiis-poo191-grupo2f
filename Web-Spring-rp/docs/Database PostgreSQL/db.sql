CREATE DATABASE rapturepark;
create table IF NOT EXISTS RESTAURANTE
(
    ID SERIAL not null
        constraint RESTAURANTE_pk
            primary key,
    NOMBRE VARCHAR(30),
    TELEFONO VARCHAR(30),
    DIRECCION TEXT,
    DESCRIPCCION TEXT
);

CREATE TABLE IF NOT EXISTS HOTEL
(   ID SERIAL not null
        constraint HOTEL_pk
            primary key,
    NOMBRE VARCHAR(30),
    NUM_HABITACIONES INT,
    DIRECCION TEXT,
    TELEFONO VARCHAR(15),
    VALORACION DOUBLE PRECISION,
    ID_RESTAURANTE INT not null
        constraint HOTEL_RESTAURANTE_ID_fk
            references RESTAURANTE (ID)
);
CREATE TABLE IF NOT EXISTS ATRACCION
(
    ID SERIAL not null
        constraint ATRACCION_PK
            primary key,
    NOMBRE VARCHAR(30),
    EDAD_RESTRINGINDA INT,
    NIVEL_INTENSIDAD INT,
    LUGAR_DISPONIBLE TEXT,
    TIPO_ATRACCION varchar(20),
    VALORACION numeric(5,1),
    FECHA_UTILIZADA date


);
CREATE TABLE IF NOT EXISTS USUARIO(
                                      ID SERIAL not null
                                          constraint USUARIO_pk
                                              primary key,
                                      NOMBRE VARCHAR(30),
                                      APELLIDO VARCHAR(30),
                                      EDAD INT,
                                      CORREO TEXT,
                                      TELEFONO VARCHAR(30),
                                      TIPO_USUARIO VARCHAR(30),
                                      USUARIO VARCHAR(30),
                                      PASSWORD VARCHAR(30),
                                      FECHA_REGISTRADO date,
                                      FECHA_MODIFICADO date
);
alter table usuario drop column fecha_registrado;

alter table usuario
    add fecha_registrado timestamp without time zone default ('now'::text)::timestamp(6) with time zone not null;
alter table usuario
    add lineabancaria varchar(30);

CREATE TABLE IF NOT EXISTS PRODUCTO(
                                       ID SERIAL not null
                                           constraint PRODUCTO_pk
                                               primary key,
                                       TIPO_PRODUCTO TEXT,
                                       NOMBRE_PRODUCTO TEXT not null ,
                                       COSTO REAL not null ,
                                       DESCRIPCION TEXT not null
);

CREATE TABLE IF NOT EXISTS PAGO(
                                   ID serial not null
                                       constraint PAGO_pk
                                           primary key,
                                   TIPO_PAGO varchar(30),
                                   MONTO REAL,
                                   FECHA_PAGO date,
                                   NUM_TRASACCION varchar(30),
                                   CANTIDAD_PRODUCTO INT,
                                   ID_USUARIO INT not null
                                       constraint PAGO_USUARIO_ID_fk
                                           references USUARIO (ID),
                                   ID_PRODUCTO INT not null
                                       constraint PAGO_PRODUCTO_ID_fk
                                           references PRODUCTO (ID)
);
create table IF NOT EXISTS visitas
(
    id serial
        constraint visitas_pk
            primary key,
    idAtraccion int not null
        constraint visitas_atraccion_id_fk
            references atraccion,
    valoracion smallint,
    momento_entrada timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL,
    momento_salida timestamp without time zone DEFAULT ('now'::text)::timestamp(6) with time zone NOT NULL
);
create table IF NOT EXISTS visitantes
(
    id serial not null
        constraint visitantes_pk
            primary key,
    tempoActivo int not null
);
alter table visitantes
    add hora_entrada timestamp without time zone default ('now'::text)::timestamp(6) with time zone;
alter table atraccion rename column valoracion to valoraciontotal;

alter table atraccion alter column valoraciontotal type numeric(2,1) using valoraciontotal::numeric(2,1);

alter table atraccion drop column fecha_utilizada;

alter table atraccion
    add numvaloraciones int;

create table product_information
(
    product_id serial not null
        constraint product_information_pk
            primary key,
    nombre_producto VARCHAR(60),
    tipo_producto VARCHAR(60),
    inventario integer,
    ofertas text,
    id_vendedor integer,
    nombre_producto_ingles varchar(60),
    lugardefabricacion varchar(60),
    list_price numeric(5,2)
);
create table costeartickets
(
    id serial not null
        constraint costeartickets_pk
            primary key,
    n_ninos integer,
    n_adultos integer,
    n_ancianos integer,
    precio numeric(6,2)
);
create table login
(
    email VARCHAR(70),
    password VARCHAR(70)
);