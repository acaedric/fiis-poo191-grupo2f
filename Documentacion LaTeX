\documentclass[12pt]{article}
\usepackage[utf8x]{inputenc}
\usepackage[spanish]{babel}
\usepackage{url}
\usepackage{lipsum}
\usepackage{graphicx}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
\title{
\vspace{-30mm}\begin{figure}[h]
\centering
\includegraphics[width=0.8in]{logoUD}
\label{escudo}
\end{figure}INGENIERÍA DE SISTEMAS\\$<$PÁGINA WEB$>$\\\Large Universidad Nacional de Ingeniería\\Proyecto Final de 'POO'}
\author{$<$Marcial Enrique Vásquez Rubio$>$\\$<$20170145D$>$\\$<$Chrystian Alexander Catro Tineo$>$\\$<$20172593D$>$\\$<$José Luis Gabriel Cruz$>$\\$<$20172641I$>$\\$<$José Antonio Bustamante Huanca$>$\\$<$20171225A$>$}



\begin{document}
\maketitle
\section{Introducción}
\subsection{Presentación y Objetivos}
 Este documento describe el trabajo que se está realizando para el proyecto final del curso Programación Orientada a Objetos brindado en la Universidad Nacional de Ingeniería(Perú), carrera de Ingeniería de Sistemas. El objetivo de este proyecto es el desarrollo de la página web de un parque de atracciones autodenominado Rapture Park. El objetivo de dicha página web es dar una información completa de todas las estructuras físicas del parque de atracciones y de sus servicios, así como proporcionar una serie de funcionalidades a los internautas que ingresen a la página web.

En cuanto a los usuarios, la página web tiene varios tipos. Por un lado los usuarios no registrados, que únicamente podrán acceder a la información general del parque de atracciones. Por otro lado están los usuarios registrados que pueden acceder a una mayor funcionalidad de la aplicación. Entre estos últimos se encuentra el usuario cliente y el usuario consultor. El primero tiene acceso a la compra de los productos del parque de atracciones vía web, como también la reserva de habitaciones y servicios derivados. Mientras que el usuario consultor tiene acceso a ver la información de los clientes, tanto de toda la página para ayudar a los visitantes(usuarios no registrados) o a los clientes ante cualquier consulta.

\subsection{Contexto}
Para el sitio web, el grupo se influenció fuertemente en la páginas web del parque de Atracciones Europa Park. Además se formaron ideas para el proyecto observando las páginas web de DisneyLand, el parque Slam Park en Madrid entre otros.
\subsection{Planteamiento del problema}
El problema que se nos ha planteado es la construcción del sitio web 'ficticio', Rapture Park. La funcionalidad de la aplicación, a grandes rasgos, debía ser:

-Mostrar información de qué atracciones, hoteles y restaurantes tiene el parque 

-Permitir al cliente comprar tickets para las atracciones, estacionamiento u otro producto.

-Permitir al cliente reservar habitaciones disponibles en los hoteles situados dentro del parque.

-Permitir la interacción del cliente con los consultores mediante un chatbox.

\subsection{Estructura del documento}

El presente documento está dividido en una serie de capítulos que corresponden, básicamente, a las distintas etapas que conforman el proceso de desarrollo del proyecto. Estas etapas han sido:


Especificación de requisitos: Se redactó de una manera global una primera visión del proyecto donde señalamos los requisitos que debía cumplir. La finalidad de esta etapa es plasmar el acuerdo entre el desarrollador y el cliente acerca de las funcionalidades del proyecto. En nuestro caso el visto bueno nos lo va dando el profesor del curso.


- Análisis: Se realizó el modelado conceptual de la futura solución mediante el uso de diagramas (diagrama de clases y diagramas de casos de uso). Los modelos ayudan a visualizar como es el sistema, proporcionando plantillas que sirven de guía en la construcción de la aplicación. En esta etapa se especifica qué debe hacer la aplicacion pero no cómo debe hacerlo. 

- Diseño: Se utilizaron los elementos y modelos obtenidos durante el análisis para transformarlos en mecanismos que puedan ser utilizados en un entorno web con las características y condiciones que establecen este tipo de entornos. Se usó en este caso la herramienta Baldamiq Mockups . Tanto la etapa del análisis como la del diseno están desprovistas de código. Un buen análisis y un buen diseno son la mejor forma de llegar a producir software de calidad.

- Implementación: Se utilizarán los elementos obtenidos en el diseño para permitir la elaboración del producto o prototipo funcional, es decir, que puede ser puesto en marcha y sometido a pruebas. Para ello se consideraran las diversas tecnologías que han intervendrán en la elaboración de dicho producto. Todo lo desarrollado en las etapas del análisis y del diseño, se traducirá a código.

- Evaluación y pruebas: Esta fase se centrará en la comprobación del correcto funcionamiento del producto desarrollado mediante una serie de pruebas. Tras estas tareas podremos dar por concluido el proyecto, por lo que en último lugar mostraremos las conclusiones obtenidas y listaremos la bibliografía completa utilizada durante la realización del mismo.

\section{ESPECIFiCACION DE REQUISITOS}
\subsection{Introducción}

\subsubsection{Propósito}

El propósito de la especificación de requisitos es definir cuales son los requerimientos que debe tener la aplicación que se va a desarrollar y describir la funcionalidad del usuario a lo largo de ella.

\subsubsection{Visión global}

A continuación se realizará la descripción general del sistema desarrollado con sus funciones, características del usuario, restricciones, supuestos y dependencias. También se expondrá una especificación detallada de los requisitos detectados.

\subsubsection{Referencias}

Una guía para la realización y supervisión de proyectos final de carrera en el ámbito de la web.

Ejemplos de otros proyectos


\subsection{Descripción General}
\subsubsection{Perspectiva del producto}
La aplicación desarrollada pretende dar información general sobre Rapture-Park, así como información más específica para cada tipo de usuario que esté registrado. Se podrá acceder a la aplicación desde cualquier sistema operativo que tenga conexión a Internet utilizando un navegador web.

\subsubsection{Funciones del producto}
A continuación se muestran las funciones que conforman la aplicación, según el tipo de usuario que se encuentre conectado.

\subsubsection{Restricciones}
Al tratarse de una aplicación web, se requiere un ordenador con un navegador convencional y una conexión a Internet básica.

\subsubsection{Supuestos y dependencia}

La aplicación desarrollada trabaja al margen de cualquier hardware o software ofreciendo así un soporte multiplataforma. La única dependencia importante que podemos encontrar está relacionada con el servidor web donde se encuentre alojado nuestro portal, el cual ha de ser capaz de soportar Java y MySQL.


Menos importante pero a tener en cuenta es el hecho de que el diseño de la interfaz de la aplicación ha sido realizada sobre el navegador Firefox, de manera que algunos aspectos como los bordes redondeados de los divs y que la posición de algunos botones salga en la posición correcta dependen de si el usuario utilizar este navegador para visualizar la web o no.


\subsection{Requisitos específicos}
\subsubsection{Requerimientos funcionales}
\subsubsection{Requerimientos de interfaces externos}
\subsubsubsection{2.3.2.1Interfaces hardware}
Al tratarse de una aplicación web, se podrá visualizar sobre cualquier sistema operativo.

\subsubsubsection{2.3.2.2Interfaces software}
La aplicación funcionará en cualquier máquina con un navegador web y conexión a Internet.

\subsubsubsection{2.3.2.3Interfaces de comunicaciones}
Las comunicaciones se efectuarán siguiendo el protocolo HTTP mediante conexiones TCP/IP.

\subsection{Obligaciones del diseño}
\subsubsubsection{Estándares cumplidos}
Se han intentado cumplir los estándares de cualquier web con acceso seguro, creando un sistema de autenticación para que nadie pueda acceder a una zona de la web a la que no tiene permiso de acceso. El idioma elegido para la presentación de las páginas ha sido el castellano.
\subsubsubsection{Limitaciones hardware}
Al tratarse de una aplicación web no se requiere un hardware específico. El servidor que albergará la base de datos del sistema deberá permanecer conectado a Internet las 24 horas, puesto que este host será quien atienda las compras y reservas para cualquier usuario en todo el mundo.


\begin{thebibliography}{00}
\bibitem{b1} 
https://europapark.de
\end{thebibliography}

\end{document}