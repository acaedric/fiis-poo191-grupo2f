**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---
## INTEGRANTES DEL GRUPO 2
a. Marcial Enrique Vásquez Rubio    20170145D

b. Chrystian Alexander Catro Tineo  20172593D

c. José Antonio Bustamante Huanca   20171225A

d. José Luis Gabriel Cruz           20172641I

## Link al video-tutorial para pasar base de datos relacionales a no relacionales (PostgreSQL -> MongoDB)
https://youtu.be/noW1uddDBbE

## Link para demostración de la página web RAPTURE PARK hecha por el grupo 2 del curso de programación orientada a objetos en la Carrera de ingenieria de Sistemas de la UNI en el ciclo 2019-1
https://www.youtube.com/channel/UC7MNMLbHU8A2I0Ep1qAvjTA/live
